package com.company;

import java.util.List;

/**
 * 给定一个链表，删除链表的倒数第 n 个节点，并且返回链表的头结点。
 * <p>
 * 示例：
 * <p>
 * 给定一个链表: 1->2->3->4->5, 和 n = 2.
 * <p>
 * 当删除了倒数第二个节点后，链表变为 1->2->3->5.
 * <p>
 * 說明：
 * <p>
 * 給定的 n 保證是有效的。
 * <p>
 * 進階：
 * <p>
 * 你能嘗試使用一趟掃描實現嗎？
 */
public class DeleteLastNNode {

    static class Node {
        int value;
        Node next;

        public Node(int value, Node next) {
            this.value = value;
            this.next = next;
        }
    }

    /**
     * 採取雙重遍歷肯定是可以解決問題的，但題目要求我們一次遍歷解決問題，那我們的思路得發散一下。
     * <p>
     * 我們可以設想假設設定了雙指針p和q的話，當q指向末尾的NULL，p與q之間相隔的元素個數為n時，那麼刪除掉p的下一個指針就完成了要求。
     * <p>
     * 設置虛擬節點dummyHead指向head
     * 設定雙指針p和q，初始都指向虛擬節點dummyHead
     * 移動q，直到p與q之間相隔的元素個數為n
     * 同時移動p與q，直到q指向的為NULL
     * 將p的下一個節點指向下下個節點
     *
     * @param input
     * @return
     */
    public static Node newMyLinkedList(List<Integer> input) {
        Node head = null;
        Node pre = null;
        for (Integer v : input) {
            if (head == null) {
                head = new Node(v, null);
                pre = head;
            } else {
                Node newNode = new Node(v, null);
                pre.next = newNode;
                pre = newNode;
            }
        }
        return head;
    }

    /**
     * 嘗試使用一趟掃描實現嗎
     *
     * @param linkedList
     * @param n
     */
    public static void deleteLastN(Node linkedList, int n) {
        Node before = linkedList;
        Node forward = linkedList;
        while (before != null && forward != null) {
            for (int i = 0; i < n; ++i) {
                if (forward != null) {
                    forward = forward.next;
                } else {
                    Node next = before.next;
                    if (next!=null) {
                        before.next = next.next;
                    } else {
                        before.next = null;
                    }
                    forward = null;
                }
            }
            if (forward != null) {
                before = before.next;
            }
        }
    }


    public static String printNode(Node linked) {
        Node pivot = linked;
        StringBuilder c = new StringBuilder();
        while (pivot != null) {
            c.append(pivot.value);
            if (pivot.next != null) {
                c.append(",");
            }
            pivot = pivot.next;
        }
        return c.toString();
    }
}

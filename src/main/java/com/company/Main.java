package com.company;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Bin Packing Problem (Minimize number of used Bins)
 * Given n items of different weights and bins each of capacity c, assign each item to a bin such that number of total used bins is minimized. It may be assumed that all items have weights smaller than bin capacity.
 * Example:
 * <p>
 * Input:  wieght[]       = {4, 8, 1, 4, 2, 1}
 * Bin Capacity c = 10
 * Output: 2
 * We need minimum 2 bins to accommodate all items
 * First bin contains {4, 4, 2} and second bin {8, 2}
 * <p>
 * Input:  wieght[]       = {9, 8, 2, 2, 5, 4}
 * Bin Capacity c = 10
 * Output: 4
 * We need minimum 4 bins to accommodate all items.
 * <p>
 * Input:  wieght[]       = {2, 5, 4, 7, 1, 3, 8};
 * Bin Capacity c = 10
 * Output: 3
 */
public class Main {


    public static int efficientJanitor(List<Float> weight) {
        weight.sort(Float::compareTo);
        float max = (float) 3.0;
        float remain = max;
        int res = 1;
        for (Float aFloat : weight) {
            float v = remain - aFloat;
            if (v >= 0) {
                remain = v;
            } else {
                res++;
                remain = max - aFloat;
            }
        }
        return res;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        List<Float> weight = getTest();
        int i = efficientJanitor(weight);
        System.out.println(i);
        List<Float> test2 = getTest2();
        i = efficientJanitor(test2);
        System.out.println(i);
    }

    private static List<Float> getTest() {
        List<Float> weight = new ArrayList<>();
        weight.add(2f);
        weight.add(1f);
        weight.add(2.2f);
        return weight;
    }

    private static List<Float> getTest2() {
        List<Float> weight = new ArrayList<>();
        weight.add(2.2f);
        weight.add(1f);
        weight.add(1f);
        weight.add(3f);
        weight.add(1f);
        return weight;
    }

}
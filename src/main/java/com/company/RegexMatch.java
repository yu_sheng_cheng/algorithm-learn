package com.company;


/**
 * 题目描述
 * 给你一个字符串 s 和一个字符规律 p，请你来实现一个支持 '.' 和'*' 的正则表达式匹配。
 * <p>
 * '.' 匹配任意单个字符
 * '*' 匹配零个或多个前面的那一个元素
 * 所谓匹配，是要涵盖 整个 字符串 s 的，而不是部分字符串。
 * <p>
 * 说明:
 * <p>
 * s 可能为空，且只包含从a-z 的小写字母。
 * <p>
 * p 可能为空，且只包含从a-z 的小写字母，以及字符 .和 *。
 * <p>
 * 示例 1:
 * <p>
 * 输入:
 * s = "aa"
 * p = "a"
 * 输出: false
 * 解释: "a" 无法匹配 "aa" 整个字符串。
 * 示例 2:
 * <p>
 * 输入:
 * s = "aa"
 * p = "a*"
 * 输出: true
 * 解释: 因为 '*' 代表可以匹配零个或多个前面的那一个元素, 在这里前面的元素就是 'a'。因此，字符串 "aa" 可被视为 'a' 重复了一次。
 * 示例 3:
 * <p>
 * 输入:
 * s = "ab"
 * p = ".*"
 * 输出: true
 * 解释: ".*" 表示可匹配零个或多个（'*'）任意字符（'.'）。
 * 示例 4:
 * <p>
 * 输入:
 * s = "aab"
 * p = "c*a*b"
 * 输出: true
 * 解释: 因为 '*' 表示零个或多个，这里 'c' 为 0 个, 'a' 被重复一次。因此可以匹配字符串 "aab"。
 * 示例 5:
 * <p>
 * 输入:
 * s = "mississippi"
 * p = "mis*is*p*."
 * 输出: false
 */
public class RegexMatch {

    /**
     * 上面的暴力解法是因为没有记录答案，记忆化搜索是在 “傻搜” 的基础之上添加 “记事本”。这里我把递归的方向给改变了，当然这不是必要的，主要想说明，对于递归来说，从后往前考虑和从前往后考虑都是可行的。
     * <p>
     * 我们假设当前问题是考虑 s 的第 i 个字母，p 的第 j 个字母，所以这时的子问题是 s[0…i] 和 p[0…j] 是否匹配：
     * <p>
     * p[j] 是字母，并且 s[i] == p[j]，当前子问题成立与否取决于子问题 s[0…i-1] 和 p[0…j-1] 是否成立
     * <p>
     * p[j] 是 '.'，当前子问题成立与否取决于子问题 s[0…i-1] 和 p[0…j-1] 是否成立
     * <p>
     * p[j] 是字母，并且 s[i] != p[j]，当前子问题不成立
     * <p>
     * p[j] 是 '*'，s[i] == p[j - 1]，或者 p[j - 1] == '.'， 当前子问题成立与否取决于子问题 s[0…i-1] 和 p[0…j] 是否成立
     * <p>
     * p[j] 是 '*'，s[i] != p[j - 1]，当前子问题正确与否取决于子问题 s[0…i] 是否匹配 p[0,…j-2]0
     *
     * @param args un-useless
     */
    public static void main(String[] args) {
        var s = "adcd";
        var p = "adc.";
        showRegex(s, p);


        s = "adXcde";
        p = "ad.cde";
        showRegex(s, p);

        s = "adddddddddd";
        p = "ad.*";
        showRegex(s, p);


    }

    public static boolean showRegex(String s, String p) {
        boolean result = testRegex(s, p);
        System.out.println("s " + s + " vs \"" + p + "\" => " + result);
        return result;
    }

    private static boolean testRegex(String s, String p) {
        char[] sArray = s.toCharArray();
        char[] pArray = p.toCharArray();
        var memo = new boolean[sArray.length + 1];
        return helper(sArray, pArray, sArray.length - 1, pArray.length - 1, memo);
    }

    /**
     *
     * @param sArr
     * @param pArr
     * @param sIdx
     * @param pIdx
     * @param memo 紀錄sArr被處理過的index, 符合pattern true, otherwise false
     * @return
     */
    private static boolean helper(char[] sArr, char[] pArr, int sIdx, int pIdx, boolean[] memo) {
        if (memo[sIdx + 1]) {
            return true;
        }

        if (sIdx == -1 && pIdx == -1) {
            memo[sIdx + 1] = true;
            return true;
        }

        boolean isFirstMatching = false;

        if (sIdx >= 0 && pIdx >= 0 && (sArr[sIdx] == pArr[pIdx] || pArr[pIdx] == '.'
                || (pArr[pIdx] == '*' && (pArr[pIdx - 1] == sArr[sIdx] || pArr[pIdx - 1] == '.')))) {
            isFirstMatching = true;
        }

        if (pIdx >= 1 && pArr[pIdx] == '*') {
            // 看 s[0,...i] 和 p[0,...j-2]
            boolean zero = helper(sArr, pArr, sIdx, pIdx - 2, memo);
            // 看 s[0,...i-1] 和 p[0,...j]
            boolean match = isFirstMatching && helper(sArr, pArr, sIdx - 1, pIdx, memo);

            if (zero || match) {
                memo[sIdx + 1] = true;
            }

            return memo[sIdx + 1];
        }

        // 看 s[0,...i-1] 和 p[0,...j-1]
        if (isFirstMatching && helper(sArr, pArr, sIdx - 1, pIdx - 1, memo)) {
            memo[sIdx + 1] = true;
        }

        return memo[sIdx + 1];
    }
}
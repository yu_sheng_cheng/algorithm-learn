package com.company;

import java.util.List;

/**
 * 題目：盛最多水的容器
 * 给定 n 个非负整数 a1，a2，...，an，每个数代表坐标中的一个点 (i, ai) 。
 * 在坐标内画 n 条垂直线，垂直线 i 的两个端点分别为 (i, ai) 和 (i, 0)。
 * 找出其中的两条线，使得它们与 x 轴共同构成的容器可以容纳最多的水。
 * <p>
 * 说明：你不能倾斜容器，且 n 的值至少为 2。
 */
public class TheMostWateryContainer {

    public static void main(String[] args) {
    }

    /**
     * 思路：先将指针指向两端的直线，再选择向内移动短的那端的指针。因为若移动长的那端，那么得到的新的容量一定被当前容量小。反之移动短的那段，可能出现更大的容量。过程如下：
     * 使用 hi, hj 表示两个直线的高度，初始化的值：i=0, j=n-1
     * 面积的计算公式为：s(i, j) = min(hi, hj) * (j - i)
     * 向内移动指针会导致底边缩减 1 个单位，不论是 i 还是 j。就是和当前比应该是 (j-i)-1。
     * 若向内移动长边指针，那么 min(hi, hj) 不会变大，甚至会变小，同时 底边一定缩减，因此 S(i, j) 一定变小。
     * 若想内移动短边指针，那么 min(hi, hj) 可能会变大。因此可能会得到最大的面积
     * 因此我们可假定初始化状态为最大面积，然后逐渐移动短边，若获得更大的面积，更新它！
     *
     * @param verticals test set
     * @return max container
     */
    public static int findTheMostWateryContainer(List<Integer> verticals) {
        Integer[] ints = new Integer[verticals.size()];
        Integer[] test = verticals.toArray(ints);
        int i = 0, j = test.length - 1;
        int res = Integer.MIN_VALUE;
        while (i != j) {
            int bottom = j - 1;
            int min = Math.min(test[i], test[j]);
            int newRes = bottom * min;
            if (newRes > res) {
                res = newRes;
            }
            if (min == test[j]) {
                j = j - 1;
            } else {
                i = i + 1;
            }
        }
        return res;
    }
}
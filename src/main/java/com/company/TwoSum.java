package com.company;

import java.util.Arrays;
import java.util.List;

public class TwoSum {


    public static void main(String[] args) {
        List<Integer> integers = List.of(2, 7, 8, 3, 10, 22);
        int[] ints = twoSum(integers, 9);
        System.out.println(Arrays.toString(ints));
        ints = twoSum(integers, 24);
        System.out.println(Arrays.toString(ints));
    }

    static int[] twoSum(List<Integer> nums, int target) {
        for(int i = 0 ; i < nums.size() ; i ++){
            int complement = target - nums.get(i);
            if(nums.contains(complement)){
                return new int[]{i, nums.indexOf(complement)};
            }
        }
        return null;
    }
}

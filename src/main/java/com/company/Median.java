package com.company;

import java.util.List;

/**
 * 给定两个大小为 m 和 n 的有序数组 nums1 和 nums2 。
 * 请你找出这两个有序数组的中位数，并且要求算法的时间复杂度为 O(log(m + n))。
 * 你可以假设 nums1 和 nums2 不会同时为空。
 */
public class Median {

    public static void main(String[] args) {
        List<Integer> a = List.of(4, 5, 6, 7, 8);
        List<Integer> b = List.of(1, 2, 3, 7);
        float median = median2(a.toArray(new Integer[0]), b.toArray(new Integer[0]));
        System.out.println(median);
        a = List.of(1, 2);
        b = List.of(3, 4);
        median = median2(a.toArray(new Integer[0]), b.toArray(new Integer[0]));
        System.out.println(median);
    }


    private static float median2(Integer[] a, Integer[] b) {
        return median(a, 0, a.length - 1, b, 0, b.length - 1);
    }

    private static float median(Integer[] a, int as, int ae, Integer[] b, int bs, int be) {
        if (ae - as == 0 && be - bs == 0) {
            return (a[as] + b[bs]) / 2f;
        }
        int aMedian = (ae + as) / 2;
        int bMedian = (be + bs) / 2;
        if (a[aMedian] < b[bMedian]) {
            return median(a, Math.min(ae, aMedian + 1), ae, b, 0, Math.max(0, bMedian - 1));
        } else if (a[aMedian] > b[bMedian]) {
            return median(a, 0, Math.max(0, aMedian - 1), b, Math.min(be, bMedian + 1), be);
        } else {
            return a[aMedian];
        }
    }
}


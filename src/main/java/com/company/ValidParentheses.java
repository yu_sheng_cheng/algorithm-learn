package com.company;

import java.util.Set;
import java.util.Stack;

public class ValidParentheses {



    public static boolean validParentheses(String testStr){
        char[] testChars = testStr.toCharArray();
        Stack<Character> stack = new Stack<>();
        Set<Character> leftParentheses = Set.of('{', '[','(');
        Set<Character> rightParentheses = Set.of('}', ']',')');
        for (var c : testChars) {
            if (leftParentheses.contains(c)){
                stack.push(c);
            }
            if (rightParentheses.contains(c)) {
                Character pop = stack.pop();
                boolean same;
                switch (c) {
                    case '}' :
                        same =  pop == '{';
                        break;
                    case ']':
                        same = pop == '[';
                        break;
                    case  ')':
                        same = pop == '(';
                        break;
                    default:
                        same = false;
                }
                if (!same) {
                    return false;
                }
            }
        }
        return true;
    }
}

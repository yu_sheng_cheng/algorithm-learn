package com.company;

import java.util.ArrayList;
import java.util.List;

/**
 * LeetCode 第 15 号问题：三数之和
 * 给定一个包含 n 个整数的数组 nums，判断 nums 中是否存在三个元素 *a，b，c ，*使得 a + b + c = 0 ？找出所有满足条件且不重复的三元组。
 */
public class ThreeSum {

    public static List<List<Integer>> findThreeSum(List<Integer> testList) {
        ArrayList<List<Integer>> result = new ArrayList<>();
        testList.sort(Integer::compareTo);
        Integer[] testArr = testList.toArray(new Integer[0]);
        for (int c = 0; c < testArr.length; c++) {
            // a + b = -c
            int target = -testArr[c];
            int a = c + 1, b = testArr.length - 1;
            while (a < b) {
                int anotherPart = testArr[a] + testArr[b];
                if (anotherPart == target) {
                    result.add(List.of(testArr[c], testArr[a], testArr[b]));
                    // next number are same, ignore it ; -3 1 1 2
                    while ((a + 1) < b && (testArr[a + 1].equals(testArr[a]))) ++a;
                    // next number are same, ignore it
                    while (a < (b - 1) && (testArr[b - 1].equals(testArr[b]))) --b;
                    ++a;
                    --b;
                } else if (anotherPart < target) {
                    a++;
                } else {
                    b--;
                }
            }
        }
        return result;
    }

}

package com.company;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ThreeSumTest {

    @Test
    void findThreeSum() {
        List<Integer> integers = List.of(1, 2, 3, 4, -5, 6, 7, -8, 9);
        List<List<Integer>> result = ThreeSum.findThreeSum(new ArrayList<>(integers));
        for (var r : result) {
            System.out.println(r);
        }
    }
}
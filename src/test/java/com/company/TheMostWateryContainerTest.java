package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TheMostWateryContainerTest {

    @Test
    void findTheMostWateryContainer() {
        List<Integer> verticals = List.of(1, 8, 6, 2, 5, 4, 8, 3, 7);
        int theMostWateryContainer = TheMostWateryContainer.findTheMostWateryContainer(verticals);
        Assertions.assertEquals(49, theMostWateryContainer);

    }


    @Test
    void findTheMostWateryContainer2() {
        List<Integer> verticals = List.of(1, 9, 8, 9, 1);
        int theMostWateryContainer = TheMostWateryContainer.findTheMostWateryContainer(verticals);
        Assertions.assertEquals(18, theMostWateryContainer);

    }
}
package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class RegexMatchTest {

    @Test
    void lambdaExpressions() {
        assertTrue(RegexMatch.showRegex("adddddddddd", "ad.*"));
    }

    @Test
    void test2() {
        assertTrue(RegexMatch.showRegex("adddfgfgfadddddd", "ad.*ad.*"));
    }

}
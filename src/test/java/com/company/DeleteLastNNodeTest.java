package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DeleteLastNNodeTest {

    @Test
    void deleteLastN() {
        DeleteLastNNode.Node node = DeleteLastNNode.newMyLinkedList(List.of(1, 2, 3, 4, 5));
        DeleteLastNNode.deleteLastN(node, 2);
        String s = DeleteLastNNode.printNode(node);
        Assertions.assertEquals("1,2,3,5", s);
    }
}
package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidParenthesesTest {

    @Test
    public void validParentheses1() {
        Assertions.assertTrue(ValidParentheses.validParentheses("()"));
    }

    @Test
    public void validParentheses2() {
        Assertions.assertTrue(ValidParentheses.validParentheses("()[]{}"));
    }

    @Test
    public void validParentheses3() {
        Assertions.assertFalse(ValidParentheses.validParentheses("(]"));
    }

    @Test
    public void validParentheses4() {
        Assertions.assertFalse(ValidParentheses.validParentheses("([)]"));
    }

    @Test
    public void validParentheses5() {
        Assertions.assertTrue(ValidParentheses.validParentheses("{[]}"));
    }


}